use <Flex.scad>;

exterior_brim(r=4, h=0.2)
{
    structure(flexhull=true);
}

structure(print_flexure=false);
