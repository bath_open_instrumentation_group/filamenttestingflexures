thickness=10;
barwidth = 10;
total_length = 100;
flex_length = 10;
flex_thickness = 1.5;
flex_rad = 5;
reinforce_length = 3*barwidth;
// gap either side of fexure when held in arm;
gap=1;
//from the reinforcement to parallel with the mount hole
mechanism_length = 55;

//Calculate the effective lever length
lever_length = mechanism_length-flex_length/2;
arm_length = lever_length;
//from the top of the base to the mount hole
mechanism_height = 1.5*barwidth;
d=0.05;



flexures =[
    [
        [flex_length/2,1.5*barwidth,0],[0,0,0]
    ]
];

module mechanism()
{
    exterior_brim(r=4, h=0.2)
    {
        structure(flexhull=true);
    }

    structure();
}
//mechanism();

module arm()
{
    difference()
    {
        union()
        {
            translate([0,-barwidth-gap,0])cube([2*barwidth,2*barwidth+2*gap,thickness]);
            translate([2*barwidth-d,-barwidth/2,0])cube([arm_length-barwidth+d,barwidth,thickness]);
        }
        translate([-d,-barwidth/2-gap,-500])cube([1.5*barwidth+d,barwidth+2*gap,999]);
        translate([barwidth/2,0,barwidth/2])rotate([90,0,0])translate([0,0,-500])cylinder(r=2.5,h=999);
        translate([arm_length+barwidth/2,0,barwidth/2])rotate([90,0,0])translate([0,0,-500])cylinder(r=2.5,h=999);
    }
}
//arm();

module structure(flexhull=false,print_flexure=true,print_rigid=true)
{
    union()
    {
        if (print_rigid)
        {
            translate([-reinforce_length,-barwidth,0])
            {
                hull(){
                    translate([2*barwidth,barwidth-d,0])cube([barwidth,2*barwidth+d,thickness]);
                    cube([reinforce_length,barwidth,thickness]);
                }
                cube([total_length,barwidth,thickness]);
            }
            //horzizontal rod
            difference()
            {
                translate([flex_length,barwidth,0])
                {
                    cube([mechanism_length-flex_length+barwidth/2,barwidth,thickness]);
                }
                translate([mechanism_length,mechanism_height,0])cylinder(r=2,h=3*thickness,center=true,$fn=20);
            }
        }
        
        if (print_flexure)
        {
            for (flex = flexures)
            {
                if (flexhull)
                {
                    hull()flexure(L=flex_length,t=flex_thickness,w=thickness,r=flex_rad,pos=flex[0],rot=flex[1],t_edge=barwidth);
                }
                else
                {
                flexure(L=flex_length,t=flex_thickness,w=thickness,r=flex_rad,pos=flex[0],rot=flex[1],t_edge=barwidth);
                }
            }
        }
    }
}


module flexure(L,t,w,r,pos=[0,0,0],rot=[0,0,0],t_edge=999)
{
    translate(pos)rotate(rot)intersection(){
        translate([-L/2-d,-t_edge/2,0])cube([L+2*d,t_edge,w]);
        difference()
        {
            translate([-L/2-d,-r-t/2,0])cube([L+2*d,2*r+t,w]);
            for(i = [-1,1])
            {
                translate([0,i*(r+t/2),0])hull()
                {
                    translate([L/2-r,0,0])cylinder(r=r,h=999,center=true);
                    translate([-(L/2-r),0,0])cylinder(r=r,h=999,center=true);
                }
            }
        }
    }
}


module exterior_brim(r=4, h=0.2){
    // Add a "brim" around the outside of an object *only*, preserving holes in the object
    //children();
    
    if(r > 0) linear_extrude(h) difference(){
        offset(r) projection(cut=true) translate([0,0,-d]) children();
       
        offset(-r+d) offset(r) projection(cut=true) translate([0,0,-d]) children();
    }
}